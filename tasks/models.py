from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile (models.Model):
    KURS_SOURCES = (
        ('python', 'Python'),
        ('c_plus', 'C++'),
        ('php', 'PHP'),
        ('java', 'JAVA'),
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name='Препод')
    kurs = models.CharField(max_length=50, verbose_name='Курс', choices=KURS_SOURCES)
    
    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профиль'

    def __str__(self):
        return "%d) %s  %s" % (self.id, self.user, self.kurs)

@receiver(post_save, sender = User)
def creat_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user = instance)

@receiver(post_save, sender = User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()



class Schedule(models.Model):
    data_first = models.DateTimeField()
    data_second = models.DateTimeField()
    event = (
        ('1', '1'),
        ('2', '2'),
        ('3', '3'),
        ('4', '5'),
    )
    teacher = models.ForeignKey(User, on_delete=models.CASCADE)


