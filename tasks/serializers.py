from .models import *
from rest_framework import serializers
from django.contrib.auth.models import User


class ScheduleSerializer(serializers.ModelSerializer):

    class Meta:
        model = Schedule
        fields = '__all__'

