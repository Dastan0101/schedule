from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from tasks import views

app_name = "shedule"

urlpatterns = [
    path('shedule/add', views.shedule_add, name = 'shedule_add'),
    path('user/add', views.user_add, name = 'user_add'),
    path('schedule/', views.ScheduleList.as_view()),
    path('schedule/<int:pk>/', views.ScheduleDetail.as_view()),
]