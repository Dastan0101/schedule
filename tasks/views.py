
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from .forms import *
from .models import *
# Create your views here.
def user_add(request):
    context = {}    
    
    form = UserAdd(request.POST or None)
    
    if request.method == 'POST' and form.is_valid():
        form1 = form.save()
        context['form'] = form
        context['saved'] = True
        context['resault'] = form1
        context['title'] = 'Добавить Препода'
        return render(request, 'add_shedule.html', context)
        
    if request.method == 'GET':
        context['form'] = form
        context['title'] = 'Добавить Препода'

    return render(request, 'add_shedule.html', context)

def shedule_add(request):
    context = {}    
    context['title'] = 'Добавить Задание'

    return render(request, 'add_shedule.html', context)

from django.shortcuts import render
from rest_framework import generics
from .models import *
from .serializers import ScheduleSerializer
from django.contrib.auth.models import User
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse


class ScheduleList(generics.ListCreateAPIView):
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class ScheduleDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer

@api_view(['GET'])
def api_root(request, format=None):
    return Response({

        'schedule': reverse('schedule-list', request=request, format=format)
    })

